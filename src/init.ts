import initAudion from "./utils/audion";
import VideoProcessor, { processOptions } from "./utils/cameron";
import { clearLine, getError } from "./utils/linalizer";
import statusBar from "./utils/status";
import autoGetCameraSource from "./utils/video_source";

/**
 * The properties that the user can provide to the function
 */
type initProps = Partial<processOptions> & {
    videoSourceFile?: string;
    areaWidth: number;
    areaHeight: number;
    treshold: number;
    lineGrayscaleColor: number;
    skip: number; // override optional skip value
    coef: number; // multiplies the error value, allows to amplify/reduce or invert the correction
}

/**
 * Test functionality of the robot by running a predefined path for 3 seconds
 */
export const testRun = async (): Promise<void> => {
    let A = initAudion();
    // goto right
    A.setVolume({
        leftVolume: 1,
        rightVolume: 0,
    });
    A.play();

    // await for 3 seconds

    await new Promise((resolve) => setTimeout(resolve, 3000));
    A.setVolume({
        leftVolume: 0,
        rightVolume: 1,
    });

    // await for 3 seconds

    await new Promise((resolve) => setTimeout(resolve, 3000));
    A.setVolume({
        leftVolume: 1,
        rightVolume: 0,
    });

    // await for 3 seconds

    await new Promise((resolve) => setTimeout(resolve, 3000));
    A.setVolume({
        leftVolume: 0,
        rightVolume: 1,
    });
}

/**
 * The actual function that start processing the video and produce the audio to control the robot
 * It needs to be in a separate function because otherwise the browser would block the audio from playing
*/
const process = async (props: initProps) => {
    /**
     * Create an instance of the Audion class to control the frequency and volume of the audio signal
     */
    let A = initAudion();

    // create status bar for the user to see current data
    const status = new statusBar(document.getElementById("app")!);
    
    // create video and dont add it to the page
    const video = document.createElement("video");
    // add the video to the page if the video source file was provided
    if (props.videoSourceFile) {
        video.src = props.videoSourceFile;
    }
    // get the camera if the video source file was not provided
    autoGetCameraSource(video);

    video.addEventListener("loadedmetadata", () => {
        video.play();
    });

    // start the video
    video.autoplay = true;
    // sound shouldnt exist in video without audio track, just in case mute it
    video.muted = true;

    // create canvas element and add it to the page
    const c1 = document.createElement("canvas") as HTMLCanvasElement;
    document.getElementById("app")!.appendChild(c1);

    let original: boolean = false;

    // start the audio signal
    A.play();

    const p = new VideoProcessor(video, c1, {
        skip: props.skip ?? 10,
        cropTop: props.cropTop,
        frameCallback: (frame, videoProps) => {
            if (original) {
                return;
            }

            let sectors: boolean[][] = [];

            // if there's not enough pixel data for the whole sector, break the loop, were done
            // calculate number of rows first from the height of the video
            const targetRowC:number = Math.floor(videoProps.height / props.areaHeight);

            /**
             * Only iterate through the frame data once to calculate the average of each sector
             * keep track of the row and column index, colIndex is -1 because we increment it first
             */
            for (let i = 0, x = 0, y = 0; i < frame.data.length; i += 4, x++) {
                if (x >= videoProps.width) {
                    // keep track of x and y coordinates
                    x = 0;
                    y++;
                }

                /**
                 * Average of the RGB values
                 */
                const grayscale = (frame.data[i] + frame.data[i + 1] + frame.data[i + 2]) / 3;
                /**
                 * The column indexes of the sector
                 */
                const rowIdx = Math.floor(y / props.areaHeight);

                if (rowIdx >= targetRowC) {
                    break;
                }

                /**
                 * The column indexe of the sector
                 */
                const colIdx = Math.floor(x / props.areaWidth);

                const isBlack = grayscale < props.treshold;
                // create a new row/col if it doesn't exist
                if (sectors[rowIdx] === undefined) {
                    sectors.push([]);
                }
                if (sectors[rowIdx][colIdx] === undefined) {
                    // create a new sector and assign the grayscale value
                    sectors[rowIdx].push(isBlack);
                } else {
                    // continuously update the average of the sector by averaging the previous average with the current value
                    sectors[rowIdx][colIdx] = isBlack
                }
            }

            // clear and improve sector data accuracy
            sectors = clearLine(sectors);

            /**
             * Count of sectors in X, Y direction
             */
            const rowc: number = sectors.length,
            colc: number = sectors[0].length;

            /**
             * Direction correction (c) from averaging the line values
             * c > 0 => line is on the right half
             * c < 0 => line is on the left half
             * Coef is a constant to amplify/reduce/reverse the correction
             */
            const correction = getError(sectors) * props.coef;
            
            // set the volume of the left and right channel based on the correction value
            const leftVolume: number = 0.5 + correction,
            rightVolume: number = 0.5 - correction;

            A.setVolume({
                leftVolume,
                rightVolume,
            });

            
            status.renderState({
                rowc,
                colc,
                volumeLeft: leftVolume,
                volumeRight: rightVolume,
            });

            /**
             * Second iteration through the frame data to paint the sectors
             * props.treshold 100
             */
            for (let i = 0, x = 0, y = 0; i < frame.data.length; i += 4, x++) {
                if (x >= videoProps.width) {
                    // keep track of x and y coordinates
                    x = 0;
                    y++;
                }

                // calculate the row and column indexes for image sectors
                const rowIdx: number = Math.floor(y / props.areaHeight);
                const colIdx: number = Math.floor(x / props.areaWidth);

                // probably not enough data for the whole sector, break the loop and remove the last row
                if (rowIdx >= sectors.length || sectors[rowIdx] === undefined || colIdx >= sectors[rowIdx].length) {
                    sectors.pop();
                    break;
                }

                // assign grayscale value of the pixel
                const isBlack: boolean = sectors[rowIdx][colIdx];

                if (isBlack) {
                    // paint the sector black if found the line
                    frame.data[i] = frame.data[i + 1] = frame.data[i + 2] = props.lineGrayscaleColor;
                }
            }

            return frame;
        },
    });

    // allow the user to toggle between the processed and original video and pause/play the video
    document.addEventListener("keydown", (e) => {
        if (e.key === "o") {
            original = !original;
            p.clearCache();

            console.log("processing", original ? "original video" : "path");
        } else if (e.key === "p") {
            if (video.paused) {
                video.play();
                A = initAudion()
                A.play();
            } else {
                video.pause();
                A.stop();
            }
        }
    });
}

export default process;