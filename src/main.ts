import process, { testRun } from "./init";

import "./style/global.css";

const areaWidth = 20;
const areaHeight = 20;
const treshold = 50;
const lineGrayscaleColor = 120;
const defaultCoef = 120;

document.addEventListener("DOMContentLoaded", () => {
  const controls = document.createElement("div");
  controls.classList.add("controls");
  document.getElementById("app")!.appendChild(controls);

  const runTestButt = document.createElement("button");
  runTestButt.innerText = "Spustit test";
  runTestButt.classList.add("button");
  controls.appendChild(runTestButt);

  runTestButt.addEventListener("click", async () => {
    // remove the controls
    controls.innerHTML = "";
    await testRun();
    // reload the page to allow the user to start again
    location.reload();
  });

  // create number input for changing the coef value
  const coefInput = document.createElement("input");
  coefInput.type = "number";
  coefInput.value = defaultCoef.toString();
  coefInput.placeholder = "Koeficient korekce";
  coefInput.step = "0.1";
  coefInput.required = true;
  controls.appendChild(coefInput);
  // create button to click on for the browser to be happy
  const startButt = document.createElement("button");
  startButt.type = "submit";
  startButt.innerText = "Spustit";
  controls.appendChild(startButt);

  // start with click on button, otherwise the browser would block the audio
  startButt.addEventListener("click", async () => {
    const coef = parseFloat(coefInput.value);
    if (isNaN(coef)) {
      alert("Koeficient musí být číslo");
      return;
    }

    // remove the controls
    controls.innerHTML = "";

    await process({
      areaWidth,
      areaHeight,
      treshold,
      lineGrayscaleColor,
      skip: 5,
      cropTop: 800,
      coef,
    });
  });
});