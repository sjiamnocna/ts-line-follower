export type AudionProps = {
    frequency: number;
    leftVolume: number;
    rightVolume: number;
}

export const volumeConstraint = (value: number): number => {
    if (value < 0) {
        return 0;
    } else if (value > 1) {
        return 1;
    }

    return value;
}

/**
 * Generate sound using <Audio> at specific frequency and allow control over volume of left and right channel
 */
export class Audion {
    private audioContext: AudioContext;
    private oscillator: OscillatorNode;
    private gainNode: GainNode;
    private frequency: number;
    private leftVolume: number;
    private rightVolume: number;

    constructor({ frequency, leftVolume, rightVolume }: AudionProps) {
        this.audioContext = new AudioContext();
        this.gainNode = this.audioContext.createGain();
        this.gainNode.connect(this.audioContext.destination);
        this.oscillator = this.audioContext.createOscillator();
        this.oscillator.connect(this.gainNode);
        this.frequency = frequency;
        this.leftVolume = leftVolume;
        this.rightVolume = rightVolume;
    }

    public play() {
        this.oscillator.frequency.value = this.frequency;
        this.oscillator.type = 'square';
        this.setVolume({});
        this.oscillator.start();
    }

    /**
     *  Set the volume of the left and right channel, constrained between 0 and 1
     */
    public setVolume({ leftVolume, rightVolume }: { leftVolume?: number, rightVolume?: number }) {
        console.log("setting volume (l/r)", leftVolume, rightVolume)
        if (leftVolume) {
            this.leftVolume = volumeConstraint(leftVolume);
        }
        if (rightVolume) {
            this.rightVolume = volumeConstraint(rightVolume);
        }

        this.gainNode.gain.setValueAtTime(this.leftVolume, this.audioContext.currentTime);
        this.gainNode.gain.setValueAtTime(this.rightVolume, this.audioContext.currentTime);
    }

    public stop() {
        this.oscillator.stop();
    }
}

/**
 * Initialize the Audion class
 * If the props are not provided, the default values are used
 * @returns 
 */
const initAudion = (props: Partial<AudionProps> = {}) => {
    // merge the props with default values
    return new Audion({
        frequency: 320,
        leftVolume: 1,
        rightVolume: 1,
        ...props,
    });
}

export default initAudion;