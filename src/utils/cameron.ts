type videoProps = {
    width: number
    height: number
}

export type processOptions = {
    skip: number,
    cropTop?: number,
    cropBottom?: number,
    frameCallback: (frame: ImageData, videoProps: videoProps) => (ImageData | void)
}

type processVars = processOptions & {
    // override crop values to be numbers
    cropTop: number
    cropBottom: number
    skipCount: number
    frameCache?: ImageData
}


export default class VideoProcessor {
    video: HTMLVideoElement;

    c1: HTMLCanvasElement;
    ctx1: CanvasRenderingContext2D;

    width: number = 0;
    height: number = 0;

    processVars: processVars;

    constructor(video: HTMLVideoElement, c1:HTMLCanvasElement, options: processOptions) {
        this.video = video;

        this.processVars = {
            frameCallback: options.frameCallback,
            skip: options.skip,
            skipCount: options.skip, // track the first frame
            cropTop: options.cropTop ?? 0,
            cropBottom: options.cropBottom ?? 0
        }

        this.c1 = c1;

        const ctx1 = c1.getContext("2d");
        if (!ctx1) {
            throw new Error("Canvas context not found");
        }

        this.ctx1 = ctx1;

        // use arrow function to keep the context of this keyword
        this.video.addEventListener("play", () => {
            // get the video dimensions and resize the canvas to match the final (cropped) dimensions
            this.c1.width = this.video.videoWidth;
            this.c1.height = this.video.videoHeight - (this.processVars.cropTop + this.processVars.cropBottom);

            this.timerCallback();
        });
    }

    timerCallback() {
        if (this.video.paused || this.video.ended) {
            return;
        }
        this.computeFrame();
        
        // arrow function keeps the context of this keyword
        setTimeout(() => this.timerCallback(), 0);
    }

    clearCache() {
        this.processVars.frameCache = undefined;
    }

    computeFrame() {
        // clear the canvas to redraw the video frame
        this.ctx1.clearRect(0, this.c1.height, this.video.videoWidth, this.video.videoHeight);
        // crop the video frame to the desired area
        this.ctx1.drawImage(this.video, 0, 0, this.video.videoWidth, this.c1.height);
        const frame = this.ctx1.getImageData(0, 0, this.video.videoWidth, this.video.videoHeight);

        if (this.processVars.skipCount < this.processVars.skip) {
            // update the canvas with the original frame data or last processed cached one and skip the frame processing
            this.ctx1.putImageData(this.processVars.frameCache ?? frame, 0, 0);
            this.processVars.skipCount++;
            return;
        } else {
            // run the frame callback only if the skip count is reached
            this.processVars.skipCount = 0;
        }

        const newData = this.processVars.frameCallback(frame, {width: this.video.videoWidth, height: this.video.videoHeight});
        if (!newData) {
            // no new image data, skip updating the canvas
            return;
        }

        // cache the processed frame data
        this.processVars.frameCache = newData;
        this.ctx1.putImageData(newData, 0, 0);
    }
}