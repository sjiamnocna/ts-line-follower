type detectorData = boolean[][];

/**
 * Look for continuous black line in the data and clear non-line pixels
 * @param data
 */
export const clearLine = (data: detectorData): detectorData => {
    // walk lines from the bottom to the top, because the line is usually at the bottom
    rowLoop:
    for (let i = data.length - 1; i >= 0; i--) {
        if (!data[i] || data[i].length === 0) {
            console.error("Row data not found", i);
            break;
        }

        for (let j = 0; j < data[i].length; j++) {
            /**
             * Look for black pixels in close area, if theres more than 3, it should be a part of a line
             * - - -
             * - x -
             * - - -
            */
            let neighbors = 0;

            // line above the current one. If undefined, it's the top border probably, only check rest of the pixels
            if (data[i - 1] != undefined) {
                for (let k = -1; k <= 1; k++) {
                    const index = j + k;
                    if (index > 0 && data[i - 1][index]) {
                        neighbors++;
                    }
                }
            } else {
                // it's the top border probably, only check rest of the pixels
                neighbors += 3;
            }

            // left and right pixels are more important, so they count as 2
            if (data[i][j - 1]) {
                neighbors += 2;
            }
            // don't concat these ifs because they are not the same
            if (data[i][j + 1]) {
                neighbors += 2;
            }

            // line below the current one
            if (data[i + 1] != undefined) {
                for (let k = -1; k <= 1; k++) {
                    const index = j + k;
                    if (index > 0 && data[i + 1][index]) {
                        neighbors++;
                    }
                }
            } else {
                // it's the bottom border probably, only check rest of the pixels
                delete data[i];
                break rowLoop;
            }

            if (neighbors >= 4){
                data[i][j] = true;
                continue;
            }

            // if the pixel in original data is not neighboring a black pixel, from at least 3 directions, clear it
            if (!data[i][j]) {
                // if it's followed by black pixels in near area it should be a part of the line
                let nextBlackPointDistance = 0;

                for (let k = 1; k < 4 && k < data[i].length; k++) {
                    if (data[i][j + k]) {
                        nextBlackPointDistance = k;
                        break;
                    }
                }

                // missed pixel, it's actually a part of the line if followed by black pixels in near area
                if (data[i][j - 1] != undefined && data[i][j - 1] && nextBlackPointDistance && nextBlackPointDistance < 3) {
                    data[i][j] = true;
                }

                continue;
            }
        }
    }

    // apply the same clearing recursively
    return data;
}

/**
 * Generate symetric coefficient values used as column weights
 * 
 * @param n Count of columns
 * @param base Number that is multiplied by the column index (default: 2)
 * @returns 
 */
export const getSymetricCoefficients = (n: number, base: number = 2): number[] => {
    const floored = Math.floor(n / 2)
    const odd = n % 2;

    const arr = [];

    // create array of coefficients
    for (let i = -floored; i <= floored; i++) {
        if (!odd && i === 0){
          continue;
        }
        arr.push((i === 0 ? 1 : i * base))
    }
    
    return arr;
}

/**
 * Calculate the error of the line in the data by averaging weighted column data
 * 
 * @param data
 * @returns 
 */
export const getError = (data: detectorData): number => {
    /**
     * Symetric coefficients for all rows
     */
    const coefficients = getSymetricCoefficients(data[1].length, 3);
    /**
     * Sum of all coefficients using reduce array method
     */
    const coefsum = coefficients.reduce((collector, b) => collector + Math.abs(b), 0);
    
    /**
     * Sum of row factors
     */
    let rowFactorSum = 0

    /**
     * Complete error
     */
    let error = 0;

    for (let i = 0; i < data.length - 1; i++) {
        /**
         * Row error 
         */
        let rowError = 0;
        
        for (let j = 0; j < data[i].length; j++) {
            if (data[i][j]) {
                // if the pixel is a line, add the coefficient to the error
                rowError += coefficients[j];
            }
        }
        const rowFactor = (i + 1) * 2;
        rowFactorSum += rowFactor;

        // divide the rowError by the sum of the column weights
        // multiply it by row factor and add it to the total error
        error += rowFactor * (rowError / coefsum);
    }

    // divide the total error by the sum of the row weights
    return error / rowFactorSum;
}

/**
 * Experimental function to get the direction of the line in the data by using the centers of the line in each row and its differences
 * 
 * @param data 
 * @returns 
 */
export const getDirection = (data: detectorData): number => {
    // get centers of the line in each row
    const centers = data.map((row) => {
        let first, last;
        for (let i = 0; i < row.length; i++) {
            if (row[i]) {
                first = i;
                // go forward to find the end of the line
                for (last = i; last < row.length; last++) {
                    if (!row[last + 1]) {
                        // line ended, 
                        break;
                    }
                }
            }

            if (last !== undefined) {
                break;
            }
        }

        first = first || 0;
        last = last || row.length - 1;

        // result should be safe index in the row as of it'S always less than the row length
        return Math.round((last + first) / 2);
    });

    // get the differences between the centers to get the direction of the line
    let diff = 0;
    let factor = 0;
    for (let i = centers.length - 1; i >= 0; i--) {
        const dy = centers[i] - centers[i - 1];
        const mul = i + 1;
        factor += mul;

        // only count if differs, multiple by the distance from the center
        diff += dy > 0 ? dy * mul : 0;
    }

    return diff / factor;
};