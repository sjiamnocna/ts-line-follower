import { volumeConstraint } from "./audion";
import { getSymetricCoefficients } from "./linalizer";

export type dimensions = {
    rowc: number
    colc: number
};
export type volume = {
    volumeLeft: number
    volumeRight: number
};
export type appState = dimensions & volume;

export default class statusBar {
    appState?: appState
    statusbar: HTMLElement
    dimensions: HTMLElement
    coefs: HTMLElement
    direction: HTMLElement
    
    constructor(root: HTMLElement){
        this.statusbar = document.createElement('div');
        this.statusbar.className = 'statusbar';

        this.direction = document.createElement('p');
        this.direction.className = 'direction';

        this.coefs = document.createElement('p');
        this.coefs.className = 'coefs';

        this.dimensions = document.createElement('p');
        this.dimensions.className = 'dimensions';

        this.statusbar.appendChild(this.direction);
        this.statusbar.appendChild(this.dimensions);
        this.statusbar.appendChild(this.coefs);

        // add statusbar to the designed root
        root.appendChild(this.statusbar);
    }

    private renderDimensions(rowc: number, colc: number): void {
        if (this.appState != undefined){
            if (this.appState.rowc == rowc && this.appState.colc == colc){
                return;
            } else {
                this.appState.rowc = rowc;
                this.appState.colc = colc;
            }
        }

        this.dimensions.innerText = `${rowc} x ${colc}`;

        // render coefficients
        const coefs = getSymetricCoefficients(rowc);
        this.coefs.innerText = coefs.join(' | ');
    }
        

    private renderVolume(l:number, r:number): void {
        const left = volumeConstraint(l);
        const right = volumeConstraint(r);

        if (this.appState != undefined){
            if (this.appState.volumeLeft == left && this.appState.volumeRight == right){
                // dont render if the values are the same
                return;
            } else {
                this.appState.volumeLeft = left;
                this.appState.volumeRight = right;
            }
        }

        this.direction.innerText = `${left} : ${right}`;
    }

    renderState(state: appState){
        this.renderDimensions(state.rowc, state.colc);
        this.renderVolume(state.volumeLeft, state.volumeRight);
    }
}