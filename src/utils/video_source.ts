/**
 * Get the camera image and insert it into the video element
 */
const autoGetCameraSource = (video: HTMLVideoElement) => {
    if (video.src !== "") {
        return;
    }

    // check if the browser supports the necessary features
    if (!('mediaDevices' in navigator) || !('getUserMedia' in navigator.mediaDevices)) {
        // tell user that the browser doesn't support the necessary features and end the application with an error
        alert("This browser doesn't support the necessary features to run this application");
        throw new Error("This browser doesn't support the necessary features to run this application");
    }

    const constraints: MediaStreamConstraints = {
        video: {
            facingMode: "environment",
        },
        audio: false,
    };

    // get the camera stream and insert it into the video element
    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
        video.srcObject = stream;
    });
}

export default autoGetCameraSource;