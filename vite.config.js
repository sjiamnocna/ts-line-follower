import { defineConfig } from "vite";

export default defineConfig(({ mode }) => {
  return {
    base: mode === 'production' ? 'https://jancsi.cz/robot/' : '/',
    // other configurations...
  };
});